import java.util.Scanner;

public class Modulo {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int[] modulos = new int[42];
		int count = 0;
		for (int i = 0; i < 10; i++) {
			int val = scan.nextInt() % 42;
			if (modulos[val] == 0)
				count++;
			modulos[val]++;
		}
		System.out.println(count);
	}

}
