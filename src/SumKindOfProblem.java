import java.util.Scanner;

public class SumKindOfProblem {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int lines = input.nextInt();
		for(int i = 0; i < lines; i++) {
			int caseNum = input.nextInt();
			int n = input.nextInt();
			int s1 = n*(n+1)/2;
			int s2 = ((2*n-1)*(2*n+1)/2+1)/2;
			int s3 = (2*n)*(2*n+2)/4;
			System.out.println(caseNum+" "+s1+" "+s2+" "+s3);
		}
	}

}
