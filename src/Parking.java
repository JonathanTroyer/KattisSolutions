import java.util.Scanner;

public class Parking {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int a = input.nextInt();
		int b = input.nextInt();
		int c = input.nextInt();
		int[] arr = new int[101];
		for (int i = 0; i < 3; i++) {
			int start = input.nextInt();
			int end = input.nextInt();
			for (int j = start; j < end; j++) {
				arr[j]++;
			}
		}
		int count = 0;
		for (int i : arr) {
			if (i == 1)
				count += a;
			else if (i == 2)
				count += b * 2;
			else if (i == 3)
				count += c * 3;
		}
		System.out.println(count);
	}

}
