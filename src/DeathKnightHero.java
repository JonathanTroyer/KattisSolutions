import java.util.Scanner;

public class DeathKnightHero {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int lines = Integer.parseInt(input.nextLine());
		int count = 0;
		for (int i = 0; i < lines; i++) {
			if (!input.nextLine().contains("CD"))
				count++;
		}
		System.out.println(count);
	}

}
