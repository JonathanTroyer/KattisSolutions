import java.util.Scanner;

public class Filip {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] ab = input.nextLine().split(" ");

        String aR = "", bR = "";
        for (int i = 2; i >= 0; i--) {
            aR += ab[0].charAt(i);
            bR += ab[1].charAt(i);
        }

        int a = Integer.parseInt(aR);
        int b = Integer.parseInt(bR);

        System.out.println(Math.max(a, b));
    }
}
