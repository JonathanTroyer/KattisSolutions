import java.math.BigInteger;
import java.util.Scanner;

public class TightWords {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			while (true) {
				int n = scan.nextInt();
				int k = scan.nextInt();

				double res = 100.0;
				if (n > 1 && k > 1) {
					BigInteger[][] tri = new BigInteger[k][n + 3];
					for (int i = 0; i < k; i++) {
						tri[i][0] = BigInteger.valueOf(0);
						tri[i][n + 2] = BigInteger.valueOf(0);
					}
					for (int i = 1; i <= n + 1; i++) {
						tri[0][i] = BigInteger.valueOf(1);
					}
					for (int i = 1; i < k; i++) {
						for (int j = 1; j <= n + 1; j++) {
							tri[i][j] = tri[i - 1][j - 1].add(tri[i - 1][j]).add(tri[i - 1][j + 1]);
						}
					}
					BigInteger tightWords = BigInteger.valueOf(0);
					for (int i = 1; i <= n + 1; i++) {
						tightWords = tightWords.add(tri[k - 1][i]);
					}

					BigInteger totalWords = BigInteger.valueOf(n + 1).pow(k);
					tightWords = tightWords.multiply(BigInteger.valueOf(100)).multiply(BigInteger.valueOf(1000000000))
							.divide(totalWords);
					res = tightWords.longValue() / 1000000000.0;
				}

				System.out.printf("%.7f\n", res);
			}
		} catch (Exception e) {
		}
	}
}
