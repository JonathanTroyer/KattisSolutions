import java.util.Scanner;

public class Pot {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int loop = input.nextInt();
		int count = 0;
		for (int i = 0; i < loop; i++) {
			int x = input.nextInt();
			count += Math.pow(x / 10, x % 10);
		}
		System.out.println(count);
	}

}
