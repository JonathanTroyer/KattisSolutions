import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Veci {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		long x = scan.nextLong();

		List<Long> digits = new ArrayList<>();
		while (x >= 10) {
			digits.add(x % 10);
			x /= 10;
		}
		digits.add(x);
		int swapIndex = 0;
		for (int i = 1; i < digits.size(); i++) {
			for (int j = 0; j < i; j++) {
				if (digits.get(i) < digits.get(j)) {
					swapIndex = i;
					long tmp = digits.get(i);
					digits.set(i, digits.get(j));
					digits.set(j, tmp);
					break;
				}
			}
			if (swapIndex > 0)
				break;
		}

		if (swapIndex == 0) {
			System.out.println(0);
		} else {
			List<Long> sub = digits.subList(0, swapIndex);
			Collections.sort(sub, Collections.reverseOrder());
			List<Long> rest = digits.subList(swapIndex, digits.size());
			sub.addAll(rest);

			for (int i = sub.size() - 1; i >= 0; i--) {
				System.out.print(sub.get(i));
			}
		}
	}

}
