import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MinScalarProduct {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int cases = input.nextInt();
		for (int q = 0; q < cases; q++) {
			int vSize = input.nextInt();

			ArrayList<BigInteger> v1 = new ArrayList<>(vSize);
			for (int i = 0; i < vSize; i++) {
				v1.add(BigInteger.valueOf(input.nextLong()));
			}

			Collections.sort(v1);

			ArrayList<BigInteger> v2 = new ArrayList<>(vSize);
			for (int i = 0; i < vSize; i++) {
				v2.add(BigInteger.valueOf(input.nextInt()));
			}

			Collections.sort(v2, Collections.reverseOrder());

			BigInteger product = new BigInteger("0");
			for (int i = 0; i < vSize; i++) {
				product = product.add(v1.get(i).multiply(v2.get(i)));
			}

			System.out.println("Case #" + (q + 1) + ": " + product);
		}
	}

}
