import java.util.Scanner;

public class Tetris {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[][][] pieces = {
			{{4},{1,0,0,0}},
			{{2,0}},
			{{2,0,1},{3,-1}},
			{{1,-1,-1},{3,1}},
			{{2,0,0},{3,1},{1,-1,0},{2,-1}},
			{{2,0,0},{3,0},{2,1,1},{1,-2}},
			{{2,0,0},{3,2},{1,0,-1},{3,0}}
		};
		int lines = input.nextInt();
		int p = input.nextInt() - 1;
		int[] arr = new int[lines];
		for (int j = 0; j < lines; j++) {
			arr[j] = input.nextInt();
		}
		int sum = 0;
		for (int[] piece : pieces[p]) {
			for (int i = 0; i < arr.length; i++) {
				boolean flag = true;
				boolean skipFlag = false;
				for (int k = 1; k < piece.length; k++) {
					if (i + k >= arr.length) {
						skipFlag = true;
						break;
					}
					flag = flag && (arr[i] + piece[k] == arr[i + k]);
				}
				if (skipFlag)
					continue;
				if (flag)
					sum++;
			}
		}
		System.out.println(sum);
	}

}
