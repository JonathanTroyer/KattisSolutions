import java.util.Scanner;

public class Bishops {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		while (input.hasNextInt()) {
			int val = input.nextInt();
			if (val != 1)
				val = val * 2 - 2;
			System.out.println(val);
		}
	}
}
