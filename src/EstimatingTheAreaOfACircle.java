import java.util.Scanner;

public class EstimatingTheAreaOfACircle {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		while (true) {
			String[] inputs = input.nextLine().split(" ");
			double radius = Double.parseDouble(inputs[0]);
			long p1 = Long.parseLong(inputs[1]);
			long p2 = Long.parseLong(inputs[2]);

			if (radius == 0 && p1 == 0 && p2 == 0)
				break;

			System.out.println((Math.PI * radius * radius) + " " + (Math.pow(2 * radius, 2) * p2 / p1));
		}
	}

}
