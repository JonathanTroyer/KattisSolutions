import java.util.Scanner;

public class TheAmazingHumanCannonball {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int lines = (int) input.nextDouble();
		for (int i = 0; i < lines; i++) {
			double v = input.nextDouble();
			double d = input.nextDouble();
			double x = input.nextDouble();
			double h1 = input.nextDouble();
			double h2 = input.nextDouble();
			double t = x / (v * Math.cos(d / 180 * Math.PI));
			double h = v * t * Math.sin(d / 180 * Math.PI) - 0.5 * 9.81 * t * t;
			if (h >= h1 + 1 && h <= h2 - 1)
				System.out.println("Safe");
			else
				System.out.println("Not Safe");
		}
	}

}
