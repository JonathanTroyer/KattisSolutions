import java.util.Scanner;

public class iBoard {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		while (input.hasNextLine()) {
			char[] line = input.nextLine().toCharArray();
			int ones = 0, zeros = 0;
			for (char c : line) {
				int count = Integer.bitCount(c);
				ones += count;
				zeros += 8 - count;
				if (Integer.highestOneBit(c) == 128) {
					ones--;
				} else {
					zeros--;
				}
			}

			if (ones % 2 == 0 && zeros % 2 == 0) {
				System.out.println("free");
			} else {
				System.out.println("trapped");
			}
		}
	}

}
