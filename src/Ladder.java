import java.util.Scanner;

public class Ladder {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double h = scan.nextDouble();
		double v = scan.nextDouble();

		double res = h / Math.sin(v * Math.PI / 180);
		System.out.println((int) Math.ceil(res));
	}

}
