import java.util.Scanner;

public class LineThemUp {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int lines = Integer.parseInt(input.nextLine());
		String last = input.nextLine();
		String line = input.nextLine();
		int flag = line.compareTo(last);
		last = line;
		for (int i = 2; i < lines; i++) {
			String current = input.nextLine();
			int f = current.compareTo(last);
			if (!((flag < 0 && f < 0) || (flag > 0 && f > 0))) {
				flag = 0;
				System.out.println("NEITHER");
				break;
			}
		}
		if (flag > 0)
			System.out.println("INCREASING");
		else if (flag < 0)
			System.out.println("DECREASING");
	}

}
