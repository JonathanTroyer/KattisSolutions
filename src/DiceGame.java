import java.util.Scanner;

public class DiceGame {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double a = (input.nextDouble() + input.nextDouble()) / 2.0 + (input.nextDouble() + input.nextDouble()) / 2.0;
		double b = (input.nextDouble() + input.nextDouble()) / 2.0 + (input.nextDouble() + input.nextDouble()) / 2.0;
		if (a > b)
			System.out.println("Gunnar");
		else if (a < b)
			System.out.println("Emma");
		else
			System.out.println("Tie");
	}

}
