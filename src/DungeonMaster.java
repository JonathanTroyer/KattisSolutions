import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DungeonMaster {

	private static class Triple {
		public int x, y, z;

		public Triple(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public boolean equals(Object other) {
			if (other instanceof Triple) {
				Triple t = (Triple) other;
				return this.x == t.x && this.y == t.y && this.z == t.z;
			}
			return false;
		}

		@Override
		public int hashCode() {
			return Integer.parseInt(x + "" + y + "" + z);
		}
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean hasNext = true;
		do {
			String[] lrc = input.nextLine().split(" ");
			final int l = Integer.parseInt(lrc[0]);
			final int r = Integer.parseInt(lrc[1]);
			final int c = Integer.parseInt(lrc[2]);
			hasNext = !(l == 0 && r == 0 && c == 0);
			if (!hasNext) {
				break;
			}

			char[][][] maze = new char[l][r][c];
			Triple startPos = null;
			boolean foundStart = false;
			// Read maze
			for (int i = 0; i < l; i++) {
				for (int j = 0; j < r; j++) {
					maze[i][j] = input.nextLine().toCharArray();
					if (!foundStart) {
						for (int k = 0; k < c; k++) {
							if (maze[i][j][k] == 'S') {
								startPos = new Triple(i, j, k);
								foundStart = true;
								break;
							}
						}
					}
				}
				input.nextLine();
			}

			Set<Triple> old = new HashSet<>();
			Set<Triple> finding = new HashSet<>();
			Set<Triple> next;
			boolean foundEnd = false;
			finding.add(startPos);
			int steps = 0;
			// Flood
			while (!foundEnd) {
				next = new HashSet<>();
				steps++;
				for (Triple node : finding) {
					// Check left/right
					if (node.z < c - 1 && maze[node.x][node.y][node.z + 1] != '#'
							&& !old.contains(new Triple(node.x, node.y, node.z + 1))) {
						if (maze[node.x][node.y][node.z + 1] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x, node.y, node.z + 1));
						}
					}
					if (node.z > 0 && maze[node.x][node.y][node.z - 1] != '#'
							&& !old.contains(new Triple(node.x, node.y, node.z - 1))) {
						if (maze[node.x][node.y][node.z - 1] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x, node.y, node.z - 1));
						}
					}
					// Check forward/back
					if (node.y < r - 1 && maze[node.x][node.y + 1][node.z] != '#'
							&& !old.contains(new Triple(node.x, node.y + 1, node.z))) {
						if (maze[node.x][node.y + 1][node.z] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x, node.y + 1, node.z));
						}
					}
					if (node.y > 0 && maze[node.x][node.y - 1][node.z] != '#'
							&& !old.contains(new Triple(node.x, node.y - 1, node.z))) {
						if (maze[node.x][node.y - 1][node.z] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x, node.y - 1, node.z));
						}
					}
					// Check up/down
					if (node.x < l - 1 && maze[node.x + 1][node.y][node.z] != '#'
							&& !old.contains(new Triple(node.x + 1, node.y, node.z))) {
						if (maze[node.x + 1][node.y][node.z] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x + 1, node.y, node.z));
						}
					}
					if (node.x > 0 && maze[node.x - 1][node.y][node.z] != '#'
							&& !old.contains(new Triple(node.x - 1, node.y, node.z))) {
						if (maze[node.x - 1][node.y][node.z] == 'E') {
							foundEnd = true;
							break;
						} else {
							next.add(new Triple(node.x - 1, node.y, node.z));
						}
					}
				}
				if (!old.addAll(finding)) {
					break;
				}
				finding = next;
			}

			if (foundEnd) {
				System.out.println("Escaped in " + steps + " minute(s).");
			} else {
				System.out.println("Trapped!");
			}

		} while (hasNext);

	}

}
