import java.util.Scanner;

public class SevenWonders {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char[] cards = input.nextLine().toCharArray();
		int t = 0, c = 0, g = 0;
		for (char ch : cards) {
			if (ch == 'T')
				t++;
			else if (ch == 'C')
				c++;
			else if (ch == 'G')
				g++;
		}
		System.out.println(t * t + c * c + g * g + 7 * Math.min(Math.min(t, c), g));
	}

}
