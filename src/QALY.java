import java.util.Scanner;

public class QALY {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String line = input.nextLine();
        int n = Integer.parseInt(line);

        float qaly = 0;
        for (int i = 0; i < n; i++) {
            line = input.nextLine();
            String[] qy = line.split(" ");
            float q = Float.parseFloat(qy[0]);
            float y = Float.parseFloat(qy[1]);
            qaly += q * y;
        }
        System.out.println(qaly);
    }
}
