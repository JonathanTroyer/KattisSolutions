import java.math.BigInteger;
import java.util.Scanner;

public class WizardOfOdds {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[] inputs = input.nextLine().split(" ");
		BigInteger x = new BigInteger(inputs[0]);
		BigInteger y = new BigInteger(inputs[1]);
		BigInteger large = new BigInteger("2");
		large = large.pow(y.intValue());
		if (x.compareTo(large) <= 0)
			System.out.println("Your wish is granted!");
		else
			System.out.println("You will become a flying monkey!");
	}

}
