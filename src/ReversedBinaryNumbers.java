import java.util.Scanner;

public class ReversedBinaryNumbers {

	public static void main(String[] args) {
		System.out.println(Integer.parseInt(new StringBuffer(Integer.toBinaryString((new Scanner(System.in)).nextInt()).toString()).reverse().toString(), 2));
	}
}
