import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class T9Spelling {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Map<Character, String> map = new HashMap<Character, String>();
		map.put('a', "2");
		map.put('b', "22");
		map.put('c', "222");
		map.put('d', "3");
		map.put('e', "33");
		map.put('f', "333");
		map.put('g', "4");
		map.put('h', "44");
		map.put('i', "444");
		map.put('j', "5");
		map.put('k', "55");
		map.put('l', "555");
		map.put('m', "6");
		map.put('n', "66");
		map.put('o', "666");
		map.put('p', "7");
		map.put('q', "77");
		map.put('r', "777");
		map.put('s', "7777");
		map.put('t', "8");
		map.put('u', "88");
		map.put('v', "888");
		map.put('w', "9");
		map.put('x', "99");
		map.put('y', "999");
		map.put('z', "9999");
		map.put(' ', "0");

		int cases = Integer.parseInt(input.nextLine());
		for (int i = 1; i <= cases; i++) {
			char[] letters = input.nextLine().toCharArray();
			int lastKey = 0;
			String keyPresses = "";
			for (char c : letters) {
				String toAdd = map.get(c);
				int currentKey = Integer.parseInt(toAdd.charAt(0) + "");
				if (currentKey == lastKey)
					keyPresses += " ";
				keyPresses += toAdd;
				lastKey = currentKey;
			}
			System.out.println("Case #" + i + ": " + keyPresses);
		}

	}

}
