import java.util.HashMap;
import java.util.Scanner;

public class AsciiAddition {

	static char[] charSet = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+' };

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		char[] line0 = input.nextLine().toCharArray();
		char[][] lines = new char[7][line0.length];
		lines[0] = line0;
		lines[1] = input.nextLine().toCharArray();
		lines[2] = input.nextLine().toCharArray();
		lines[3] = input.nextLine().toCharArray();
		lines[4] = input.nextLine().toCharArray();
		lines[5] = input.nextLine().toCharArray();
		lines[6] = input.nextLine().toCharArray();

		String line = "";
		char[][] character = new char[7][5];
		for (int i = 0; i < line0.length; i += 6) {
			for (int j = 0; j < 7; j++) {
				for (int k = 0; k < 5; k++) {
					character[j][k] = lines[j][i + k];
				}
			}
			char c = getChar(character);
			if (c == '+') {
				c = ' ';
			}
			line += c;
		}

		String[] chunks = line.split(" ");
		String result = ("" + (Integer.parseInt(chunks[0]) + Integer.parseInt(chunks[1])));
		lines = new char[7][result.length() * 6];
		int index = 0;
		for (char c : result.toCharArray()) {
			character = getAscii(c);
			for (int i = 0; i < 7; i++) {
				for (int j = 0; j < 5; j++) {
					lines[i][j + index] = character[i][j];
				}
				lines[i][index + 5] = '.';
			}
			index += 6;
		}

		for (char[] l : lines) {
			String s = new String(l);
			System.out.println(s.substring(0, s.length() - 1));
		}
	}

	static char getChar(char[][] ascii) {
		for (char c : charSet) {
			if (arrEqual(ascii, getAscii(c))) {
				return c;
			}
		}
		return '.';
	}

	static boolean arrEqual(char[][] one, char[][] two) {
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 5; j++) {
				if (one[i][j] != two[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	static char[][] getAscii(char i) {
		switch (i) {
		case '0':
			char[][] zero = { { 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return zero;
		case '1':
			char[][] one = { { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ '.', '.', '.', '.', 'x' } };
			return one;
		case '2':
			char[][] two = { { 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', '.' }, { 'x', '.', '.', '.', '.' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return two;
		case '3':
			char[][] three = { { 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return three;
		case '4':
			char[][] four = { { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ '.', '.', '.', '.', 'x' } };
			return four;
		case '5':
			char[][] five = { { 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', '.' }, { 'x', '.', '.', '.', '.' },
					{ 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return five;
		case '6':
			char[][] six = { { 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', '.' }, { 'x', '.', '.', '.', '.' },
					{ 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return six;
		case '7':
			char[][] seven = { { 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ '.', '.', '.', '.', 'x' } };
			return seven;
		case '8':
			char[][] eight = { { 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return eight;
		case '9':
			char[][] nine = { { 'x', 'x', 'x', 'x', 'x' }, { 'x', '.', '.', '.', 'x' }, { 'x', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' }, { '.', '.', '.', '.', 'x' }, { '.', '.', '.', '.', 'x' },
					{ 'x', 'x', 'x', 'x', 'x' } };
			return nine;
		default:
			char[][] plus = { { '.', '.', '.', '.', '.' }, { '.', '.', 'x', '.', '.' }, { '.', '.', 'x', '.', '.' },
					{ 'x', 'x', 'x', 'x', 'x' }, { '.', '.', 'x', '.', '.' }, { '.', '.', 'x', '.', '.' },
					{ '.', '.', '.', '.', '.' } };
			return plus;
		}
	}

}
