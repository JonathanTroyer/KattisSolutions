import java.util.Scanner;

public class Autori {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] names = input.nextLine().split("-");

        String abbr = "";
        for(String s : names) {
            abbr += s.charAt(0);
        }

        System.out.println(abbr);
    }
}
